import Logo from '../../image/logo.svg';

import { Wrapper, Content, LogoImg } from './Header.styles';

function Header() {
  return (
    <Wrapper>
      <Content>
        <LogoImg src={Logo} alt="Logo" />
        <div>
          <h1>Questa è un'app in React</h1>
        </div>
      </Content>
    </Wrapper>
  );
}

export default Header;
