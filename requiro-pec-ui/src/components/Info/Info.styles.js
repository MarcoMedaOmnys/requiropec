import styled from 'styled-components';

export const Wrapper = styled.div`
  background: var(--medGrey);
  padding: 0 20px;
  display: flex;
  height: auto;
`;

export const Content = styled.div``;

export const Text = styled.div`
  display: flex;
  width: 100%;
  padding: 0 20px;

  span {
    font-size: var(--fontBig);
    color: var(--white);
    padding-right: 10px;
  }
`;
