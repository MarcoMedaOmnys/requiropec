import { Wrapper, Text, Content } from './Info.styles';
import { Link } from 'react-router-dom';

function Info() {
  return (
    <Wrapper>
      <Content>
        <h2>Questa è un'altra pagina</h2>
        <h3>Possibile grazie a React Router!</h3>
        <Text>
          <p>
            Torna alla{' '}
            <Link to="/">
              <span>Home</span>
            </Link>
          </p>
        </Text>
      </Content>
    </Wrapper>
  );
}

export default Info;
