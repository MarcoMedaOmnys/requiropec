import styled from 'styled-components';

export const Wrapper = styled.div`
  background: lightblue;
  padding: 0 20px;
  display: flex;
  height: 100%;
`;

export const Content = styled.div`
  font-size: 0.5rem;
  margin: 20px 0;
`;

export const LinkButton = styled.button`
  display: block;
  background: var(--darkGrey);
  width: 25px;
  min-width: 200px;
  height: 60px;
  border-radius: 60px;
  color: var(--white);
  border: 0;
  font-size: var(--fontBig);
  margin: 20px auto;
  transition: all 0.3s;
  outline: none;
  cursor: pointer;

  :hover {
    opacity: 0.8;
  }
`;
