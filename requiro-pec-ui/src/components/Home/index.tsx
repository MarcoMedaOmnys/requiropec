import { Wrapper, Content, LinkButton } from './Home.styles';
import { Link } from 'react-router-dom';

function Home() {
  return (
    <Wrapper>
      <Content>
        <h1>Questa è la home</h1>
        <Link to="/info">
          <LinkButton type="button">naviga</LinkButton>
        </Link>
      </Content>
    </Wrapper>
  );
}

export default Home;
