//styles
import { GlobalStyle } from './GlobalStyle';
//components
import Header from './components/Header';
import Home from './components/Home';
import Info from './components/Info';

//routes
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

function App() {
  return (
    <Router>
      <Header></Header>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/info" element={<Info />} />
      </Routes>
      <GlobalStyle />
    </Router>
  );
}

export default App;
